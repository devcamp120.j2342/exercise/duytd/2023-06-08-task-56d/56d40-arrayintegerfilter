package com.devcamp.d40.arrayfilterinputrestapi.services;

import java.util.*;

import org.springframework.stereotype.Service;

@Service
public class ArrayIntegerService {

    private int[] largerNumbers = { 1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18 };

    public ArrayList<Integer> getArrayIntByPos(int pos) {
        ArrayList<Integer> result = new ArrayList<>();

        for (Integer integerElement : largerNumbers) {
            if (integerElement > pos) {
                result.add(integerElement);
            }
        }
        return result;
    }

    public Integer getArrIntParam(int index) {

        if (index >= 0 && index < largerNumbers.length) {
            return largerNumbers[index];
        }
        return null;
    }

}
