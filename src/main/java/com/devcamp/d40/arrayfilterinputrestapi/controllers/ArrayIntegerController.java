package com.devcamp.d40.arrayfilterinputrestapi.controllers;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.d40.arrayfilterinputrestapi.services.ArrayIntegerService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ArrayIntegerController {
    @Autowired
    ArrayIntegerService arrayintegerService;

    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> getArrIntByPos(@RequestParam() int pos) {
        return arrayintegerService.getArrayIntByPos(pos);
    }

    @GetMapping("/array-int-param/{index}")
    public Integer getArrIntByIndex(@PathVariable() int index) {
        return arrayintegerService.getArrIntParam(index);
    }
}
